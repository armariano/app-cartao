package br.com.cartao.controllers;

import br.com.cartao.dtos.EntradaCartaoDto;
import br.com.cartao.dtos.EntradaCartaoResumidaDto;
import br.com.cartao.dtos.SaidaCartaoDto;
import br.com.cartao.models.Cartao;
import br.com.cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    public ResponseEntity<?> criarCartao(@RequestBody EntradaCartaoDto entradaCartaoDto) {
        try {
            SaidaCartaoDto saidaCartaoDto = cartaoService.criarCartao(entradaCartaoDto);
            return ResponseEntity.status(201).body(saidaCartaoDto);
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PatchMapping("/{numero}")
    public ResponseEntity<?> atualizarCartaoPorNumero(@PathVariable(name = "numero") String numero, @RequestBody EntradaCartaoResumidaDto entradaCartaoResumidaDto){
        try{
            SaidaCartaoDto saidaCartaoDto = cartaoService.atualizarCartaoPorNumero(numero, entradaCartaoResumidaDto);
            return ResponseEntity.status(200).body(saidaCartaoDto);
        }catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/{numero}")
    public ResponseEntity<?> buscarCartaoPorNumero(@PathVariable(name = "numero") String numero){
        try{
            SaidaCartaoDto saidaCartaoDto = cartaoService.buscarCartaoPorNumero(numero);
            return ResponseEntity.status(200).body(saidaCartaoDto);
        }catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

}
