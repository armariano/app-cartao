package br.com.cartao.controllers;

import br.com.cartao.dtos.EntradaPagamentoDto;
import br.com.cartao.dtos.SaidaCartaoDto;
import br.com.cartao.dtos.SaidaPagamentoDto;
import br.com.cartao.models.Pagamento;
import br.com.cartao.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping
    public ResponseEntity<?> criarPagamento(@RequestBody EntradaPagamentoDto entradaPagamentoDto){
        try {
            SaidaPagamentoDto saidaPagamentoDto = pagamentoService.criarPagamento(entradaPagamentoDto);
            return ResponseEntity.status(201).body(saidaPagamentoDto);
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/{id_cartao}")
    public ResponseEntity<?> buscarPagamentoPorNumeroCartao(@PathVariable(name = "id_cartao") Integer id_cartao){
        try{
            List<SaidaPagamentoDto> pagamentos = pagamentoService.buscarPagamentoPorNumeroCartao(id_cartao);
            return ResponseEntity.status(200).body(pagamentos);
        }catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
