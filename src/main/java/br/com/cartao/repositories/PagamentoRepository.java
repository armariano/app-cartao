package br.com.cartao.repositories;

import br.com.cartao.models.Cartao;
import br.com.cartao.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {

    List<Pagamento> findAllByCartao(Cartao cartao);
}
