package br.com.cartao.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class EntradaCartaoDto {

    public EntradaCartaoDto() {
    }

    public EntradaCartaoDto(@NotBlank(message = "numero não pode ser vazio") @NotNull(message = "numero não pode ser nulo") String numero, @NotBlank(message = "clienteId não pode ser vazio") @NotNull(message = "clienteId não pode ser nulo") Integer clienteId) {
        this.numero = numero;
        this.clienteId = clienteId;
    }

    @NotBlank(message = "numero não pode ser vazio")
    @NotNull(message = "numero não pode ser nulo")
    private String numero;

    @NotBlank(message = "clienteId não pode ser vazio")
    @NotNull(message = "clienteId não pode ser nulo")
    private Integer clienteId;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }
}
