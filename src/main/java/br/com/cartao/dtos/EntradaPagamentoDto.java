package br.com.cartao.dtos;

public class EntradaPagamentoDto {

    public EntradaPagamentoDto() {
    }

    public EntradaPagamentoDto(Integer cartao_id, String descricao, Double valor) {
        this.cartao_id = cartao_id;
        this.descricao = descricao;
        this.valor = valor;
    }

    public Integer cartao_id;

    public String descricao;

    public Double valor;

    public Integer getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(Integer cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
