package br.com.cartao.dtos;

public class EntradaCartaoResumidaDto {

    public EntradaCartaoResumidaDto() {
    }

    public EntradaCartaoResumidaDto(Boolean ativo) {
        this.ativo = ativo;
    }

    public Boolean ativo;

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
