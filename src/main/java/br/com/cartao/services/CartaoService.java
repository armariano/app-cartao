package br.com.cartao.services;

import br.com.cartao.dtos.EntradaCartaoDto;
import br.com.cartao.dtos.EntradaCartaoResumidaDto;
import br.com.cartao.dtos.SaidaCartaoDto;
import br.com.cartao.models.Cartao;
import br.com.cartao.models.Cliente;
import br.com.cartao.repositories.CartaoRepository;
import br.com.cartao.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    public SaidaCartaoDto criarCartao(EntradaCartaoDto entradaCartaoDto){
        Optional<Cliente> clienteOptional = clienteRepository.findById(entradaCartaoDto.getClienteId());
        Cartao cartaoRetorno = new Cartao();
        if(clienteOptional.isPresent()){
            Cartao cartao = new Cartao();
            Cliente cliente = new Cliente();
            cliente = clienteOptional.get();
            cartao.setCliente(cliente);
            cartao.setNumero(entradaCartaoDto.getNumero());
            cartao.setAtivo(Boolean.TRUE);

            cartaoRetorno = cartaoRepository.save(cartao);

            SaidaCartaoDto saidaCartaoDto = new SaidaCartaoDto();
            saidaCartaoDto.setAtivo(cartaoRetorno.getAtivo());
            saidaCartaoDto.setClienteId(cartaoRetorno.getCliente().getId());
            saidaCartaoDto.setNumero(cartaoRetorno.getNumero());
            saidaCartaoDto.setId(cartaoRetorno.getId());

            return saidaCartaoDto;
        }
        throw new RuntimeException("Cliente não encontrado");
    }

    public SaidaCartaoDto atualizarCartaoPorNumero(String numero, EntradaCartaoResumidaDto entradaCartaoResumidaDto){

        SaidaCartaoDto saidaCartaoDto = new SaidaCartaoDto();
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);

        if(cartaoOptional.isPresent()){
            Cartao cartao = new Cartao();
            cartao = cartaoOptional.get();

            cartao.setAtivo(entradaCartaoResumidaDto.getAtivo());

            Cartao cartaoRetorno = cartaoRepository.save(cartao);
            saidaCartaoDto.setId(cartaoRetorno.getId());
            saidaCartaoDto.setNumero(cartaoRetorno.getNumero());
            saidaCartaoDto.setClienteId(cartaoRetorno.getCliente().getId());
            saidaCartaoDto.setAtivo(cartaoRetorno.getAtivo());

            return saidaCartaoDto;

        }
        throw new RuntimeException("Cartão não encontrado");
    }

    public SaidaCartaoDto buscarCartaoPorNumero(String numero){
        SaidaCartaoDto saidaCartaoDto = new SaidaCartaoDto();
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);
        if(cartaoOptional.isPresent()){
            Cartao cartao = new Cartao();
            cartao = cartaoOptional.get();

            saidaCartaoDto.setId(cartao.getId());
            saidaCartaoDto.setNumero(cartao.getNumero());
            saidaCartaoDto.setClienteId(cartao.getCliente().getId());
            saidaCartaoDto.setAtivo(cartao.getAtivo());

            return saidaCartaoDto;

        }
        throw new RuntimeException("Cartao não encontrado");
    }

}
