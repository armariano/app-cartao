package br.com.cartao.services;

import br.com.cartao.models.Cliente;
import br.com.cartao.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criarCliente(Cliente cliente){
        Cliente retornoCliente = clienteRepository.save(cliente);
        return retornoCliente;
    }

    public Cliente buscarClientePorId(int id){
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);
        if(clienteOptional.isPresent()){
            return clienteOptional.get();
        }
        throw new RuntimeException("Cliente não encontrado");
    }

}
