package br.com.cartao.services;

import br.com.cartao.dtos.EntradaPagamentoDto;
import br.com.cartao.dtos.SaidaPagamentoDto;
import br.com.cartao.models.Cartao;
import br.com.cartao.models.Pagamento;
import br.com.cartao.repositories.CartaoRepository;
import br.com.cartao.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoRepository cartaoRepository;

    public SaidaPagamentoDto criarPagamento(EntradaPagamentoDto entradaPagamentoDto){
        Optional<Cartao> optionalCartao = cartaoRepository.findById(entradaPagamentoDto.getCartao_id());

        if(optionalCartao.isPresent()){

            Cartao cartao = new Cartao();
            cartao = optionalCartao.get();

            Pagamento pagamento = new Pagamento();
            pagamento.setCartao(cartao);
            pagamento.setValor(entradaPagamentoDto.getValor());
            pagamento.setDescricao(entradaPagamentoDto.getDescricao());
            pagamento.setValor(entradaPagamentoDto.getValor());


            Pagamento pagamentoRetorno = new Pagamento();
            pagamentoRetorno = pagamentoRepository.save(pagamento);
            SaidaPagamentoDto saidaPagamentoDto = new SaidaPagamentoDto();

            saidaPagamentoDto.setId(pagamentoRetorno.getId());
            saidaPagamentoDto.setCartao_id(pagamentoRetorno.getCartao().getId());
            saidaPagamentoDto.setDescricao(pagamentoRetorno.getDescricao());
            saidaPagamentoDto.setValor(pagamentoRetorno.getValor());


            return saidaPagamentoDto;
        }
        throw new RuntimeException("Cartão não encontrado");
    }

    public List<SaidaPagamentoDto> buscarPagamentoPorNumeroCartao(Integer cartao_id){
        Optional<Cartao> optionalCartao = cartaoRepository.findById(cartao_id);
        if(optionalCartao.isPresent()){

            Cartao cartao = new Cartao();
            cartao = optionalCartao.get();

            List<Pagamento> pagamentos = pagamentoRepository.findAllByCartao(cartao);
            List<SaidaPagamentoDto> saidaPagamentoDtos = new ArrayList<SaidaPagamentoDto>();

            for (Pagamento pg: pagamentos) {
                SaidaPagamentoDto saidaPagamentoDto = new SaidaPagamentoDto();

                saidaPagamentoDto.setValor(pg.getValor());
                saidaPagamentoDto.setDescricao(pg.getDescricao());
                saidaPagamentoDto.setCartao_id(pg.getCartao().getId());
                saidaPagamentoDto.setId(pg.getId());

                saidaPagamentoDtos.add(saidaPagamentoDto);
            }

            return saidaPagamentoDtos;

        }
        throw new RuntimeException("Cartão não encontrado");
    }
}
