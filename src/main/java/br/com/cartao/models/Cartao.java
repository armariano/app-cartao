package br.com.cartao.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class Cartao {

    public Cartao() {
    }

    public Cartao(int id, @NotNull(message = "numero não pode ser nulo") @NotBlank(message = "numero não pode ser vazio") String numero, @NotNull(message = "status não pode ser nulo") @NotBlank(message = "status não pode ser vazio") Boolean ativo, Cliente cliente) {
        this.id = id;
        this.numero = numero;
        this.ativo = ativo;
        this.cliente = cliente;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "numero não pode ser nulo")
    @NotBlank(message = "numero não pode ser vazio")
    private String numero;

    @NotNull(message = "status não pode ser nulo")
    private Boolean ativo;

    @ManyToOne(cascade = CascadeType.ALL)
    private Cliente cliente;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
